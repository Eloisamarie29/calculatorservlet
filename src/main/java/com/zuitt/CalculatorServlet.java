package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {
	
	public void init()throws ServletException {
		System.out.println("***************************");
		System.out.println("Initialize connection to database.");
		System.out.println("***************************");
		
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		//The getWriter methods is use to print out information in the browser as a response
		PrintWriter out = res.getWriter();
		
		/*
		 * The Parameter name are define in the form of input field.
		 * The parameter are found in the url query string 
		 * getParameter()
		 * */
		
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		String operation = req.getParameter("operation");
		
		int total = 0;
		
		//This is where the operation on a basic calculator implemented 
		
		if(operation.equals("add")) {		
			total = num1 + num2;		
		}
		else if(operation.equals("subtract")) {			
			total = num1 - num2;			
		}
		else if(operation.equals("multiply")) {		
			total = num1 * num2;		
		}
		
		else if(operation.equals("divide")) {		
			total = num1 / num2;	
		}
		
		out.printf("The two numbers you provided are: " +num1+ " , " +num2);
		out.printf("\n\nThe operation that you wanted is: " + operation);
		out.printf("\n\nThe result is: " + total);
	}
		
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		PrintWriter out = res.getWriter();
		// Instruction needed to operate the application 
		out.println("<h1>You are now using the calculator app</h1>");
		out.println("To use the app, input 2 numbers and an operation.");
		out.println("<br><br>Hit the submission button after filling in the details.");
		out.println("<br><br>You will get the result shown in your browser!");
	}
	
	//Destroys the application 
	public void destroy() {
		
		System.out.println("***************************");
		System.out.println("Desconected from database.");
		System.out.println("***************************");
		
	}
}
